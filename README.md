# Appolo
Este repositório contém todos os arquivos estáticos (HTML, CSS e Javascript) do [website da Appolo](http://appolo.com.br).

## Hospedagem
Os arquivos estão hospedados em 2 locais diferentes:

### - Aqui no [Bitbucket](https://appolo.bitbucket.io)
O Bitbucket oferece uma hospedagem bem simples de arquivos estáticos, mas a habilidade de configurar um domínio customizável foi descontinuada à alguns anos e os sites aqui só podem ser acessados através do do link [nome_de_usuário].bitbucket.io (que não é bonito né).

### - Na [Netlify](https://app.netlify.com/sites/appolo/overview)
A Netlify também é um serviço de hospedagem de sites estáticos mas com algumas vantagens:

* A capacidade de configurar um domínio personalizado (por isso que eu usei ele).
* Rodas testes unitários (configurados por mim) para checar a saúde do website.
* Fazer o deploy automático do site após a atualização dos arquivos.
* [https://www.netlify.com/features/](https://www.netlify.com/features/).

**Observação:** Todas as alterações feitas na branch master@HEAD neste repositório são aplicadas automaticamente lá na Netlify (ou seja, as alterações são enviar imediatamente para o website http://appolo.com.br).

## No futuro
Atualmente como o website é estático ele não necessita da contratação de um servidor como a Umbler para rodar códigos de Back-end, mas posteriormente, se o site precisar de um sistema mais complexo para geração de páginas dinámicas, a migração do dóminio na Netlify para a Umbler ou Google Cloud deverá ser feita.